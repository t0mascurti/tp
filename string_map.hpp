
#include "string_map.h"

template<typename T>
string_map<T>::string_map() {
    raiz = nullptr;
    _size = 0;

}

template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar) : string_map() {
    *this = aCopiar;
}

template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    if (d.raiz == nullptr) {
    } else {
        igual(d.raiz, "");
    }
}

template<typename T>
string_map<T>::~string_map() {
    if (_size == 0) {
        return;
    }
    elim(raiz);
    return;
}

template<typename T>
T &string_map<T>::operator[](const string &clave) {

}


template<typename T>
int string_map<T>::count(const string &clave) const {
    int encontrado = 1;
    if (raiz == nullptr) {
        return 0;
    }
    if (raiz->siguientes[int(clave[0])] == nullptr) {
        return 0;
    } else {
        Nodo *actual = raiz;
        for (char a: clave) {
            if (encontrado == 1) {
                if (actual->siguientes[int(a)] != nullptr) {
                    actual = actual->siguientes[int(a)];
                } else {
                    return 0;
                }
            }
        }
        if (actual->definicion == nullptr) {
            return 0;
        }
        return encontrado;
    }
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo *actual = raiz;
    for (char a:clave) {
        actual = actual->siguientes[int(a)];
    }
    T res = (*actual->definicion);
    return res;
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo *actual = raiz;
    for (char a:clave) {
        actual = actual->siguientes[int(a)];
    }
    return *actual->definicion;
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    _size--;
    eliminar(clave, true);
}

template<typename T>
int string_map<T>::size() const {
    int res = _size;
    return res;
}

template<typename T>
bool string_map<T>::empty() const {
    return _size == 0;
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &p) {
    if (raiz == nullptr) {
        raiz = new Nodo();
    }
    Nodo *actual = raiz;
    for (char a:p.first) {
        if (actual->siguientes[int(a)] == nullptr) {
            actual->siguientes[int(a)] = new Nodo;
            actual = actual->siguientes[int(a)];
        } else {
            actual = actual->siguientes[int(a)];
        }
    }
    if (actual->definicion == nullptr) {
        _size++;
        actual->definicion = new T(p.second);
    } else {
        delete actual->definicion;
        actual->definicion = new T(p.second);
    }
}


template<typename T>
bool string_map<T>::esHoja(string_map::Nodo *&a) {
    bool res = true;
    for (Nodo *hijo: a->siguientes) {
        if (hijo != nullptr) {
            res = false;
        }
    }
    return res;
}

template<typename T>
void string_map<T>::eliminar(string clave, bool e) {
    Nodo *actual = raiz;
    Nodo *prev= raiz;
    char b;
    if (clave.size() > 0) {
        for (char a: clave) {
            prev= actual;
            b=a;
            actual = actual->siguientes[int(a)];
        }
        if (esHoja(actual)) {                   // No tiene hijos
            if(actual->definicion==nullptr) {
                //delete actual->definicion;
                prev->siguientes[int(b)]=nullptr;
                delete actual;
                //clave.erase(clave.end(), clave.end());
                clave.pop_back();
                eliminar(clave, false);
            } else{
                if(e){
                    delete actual->definicion;
                    delete actual;
                    prev->siguientes[int(b)]=nullptr;
                    //clave.erase(clave.end(), clave.end());
                    clave.pop_back();
                    eliminar(clave, false);
                }
            }
        } else {                                    // tiene hijos
            delete actual->definicion;
            actual->definicion = nullptr;
        }
    }
}

template<typename T>
void string_map<T>::elim(string_map::Nodo *r) {
    if (esHoja(r)) {
        if (r->definicion != nullptr) {
            delete r->definicion;
            _size--;
        }
        delete r;
    } else {
        for (int i = 0; i < 256; ++i) {
            if (r->siguientes[i] != nullptr) {
                elim(r->siguientes[i]);
            }
        }
        if (r->definicion != nullptr) {
            delete r->definicion;
            _size--;
        }
        delete r;
    }
}

template<typename T>
void string_map<T>::igual(string_map::Nodo *r, string rama) {
    if (esHoja(r)) {
        insert(make_pair(rama, *r->definicion));
    } else {
        if (r->definicion != nullptr) {
            insert(make_pair(rama, *r->definicion));
        }
        for (int i = 0; i < 256; ++i) {
            if (r->siguientes[i] != nullptr) {
                rama.push_back(char(i));
                igual(r->siguientes[i], rama);
                rama.pop_back();
            }
        }
    }
}



